let shortSample = [
  {
  "conclusion": {
      "description": "",
      "title": "notckd "
  },
  "description": "J48",
  "title": "Rule2",
  "conditions": [
      { 
      "description": "",
      "key": "sg",
      "value": "1.015",
      "operator": ">"
      },
      {
      "description": "",
      "key": "al",
      "value": "0",
      "operator": "<="
      },
      {
      "description": "",
      "key": "bp",
      "value": "80",
      "operator": "<="
      }
  ]
  },
  {
  "conclusion": {
      "description": "",
      "title": "ckd "
  },
  "description": "J48",
  "title": "Rule3",
  "conditions": [
      {
      "description": "",
      "key": "sg",
      "value": "1.015",
      "operator": ">"
      },
      {
      "description": "",
      "key": "al",
      "value": "0",
      "operator": "<="
      },
      {
      "description": "",
      "key": "bp",
      "value": "80",
      "operator": ">"
      }
  ]
  },
  {
    "conclusion": {
      "description": "",
      "title": " notckd (77.07/6.05)"
    },
    "description": "PART Classifier",
    "title": "Rule2",
    "conditions": [
      {
        "description": "",
        "key": "al",
        "value": "0",
        "operator": "<="
      },
      {
        "description": "",
        "key": "sg",
        "value": "1.02",
        "operator": ">"
      },
      {
        "description": "",
        "key": "bp",
        "value": "80",
        "operator": "<="
      }
    ]
  },


  {
    "conclusion": {
      "description": "",
      "title": " notckd (88.84/12.83)"
    },
    "description": "PART Classifier",
    "title": "Rule3",
    "conditions": [
      {
        "description": "",
        "key": "al",
        "value": "0",
        "operator": "<="
      },
      {
        "description": "",
        "key": "bp",
        "value": "80",
        "operator": "<="
      }
    ]
  },
  {
    "conclusion": {
      "description": "",
      "title": "ckd "
    },
    "description": "Random Forest",
    "title": "Rule1",
    "conditions": [
      {
        "description": "",
        "key": "pc",
        "value": "normal",
        "operator": "="
      },
      {
        "description": "",
        "key": "sg",
        "value": "1.02",
        "operator": "<"
      },
      {
        "description": "",
        "key": "bp",
        "value": "65",
        "operator": "<"
      },
      {
        "description": "",
        "key": "age",
        "value": "",
        "operator": "< 69.5"
      }
    ]
  },
  {
    "conclusion": {
      "description": "",
      "title": "notckd "
    },
    "description": "Random Forest",
    "title": "Rule2",
    "conditions": [
      {
        "description": "",
        "key": "pc",
        "value": "normal",
        "operator": "="
      },
      {
        "description": "",
        "key": "sg",
        "value": "1.02",
        "operator": "<"
      },
      {
        "description": "",
        "key": "bp",
        "value": "65",
        "operator": "<"
      },
      {
        "description": "",
        "key": "age",
        "value": "69.5",
        "operator": ">="
      },
      {
        "description": "",
        "key": "age",
        "value": "",
        "operator": "< 70.5"
      }
    ]
  },
  {
    "conclusion": {
      "description": "",
      "title": "ckd "
    },
    "description": "Random Forest",
    "title": "Rule3",
    "conditions": [
      {
        "description": "",
        "key": "pc",
        "value": "normal",
        "operator": "="
      },
      {
        "description": "",
        "key": "sg",
        "value": "1.02",
        "operator": "<"
      },
      {
        "description": "",
        "key": "bp",
        "value": "65",
        "operator": "<"
      },
      {
        "description": "",
        "key": "age",
        "value": "69.5",
        "operator": ">="
      },
      {
        "description": "",
        "key": "age",
        "value": "",
        "operator": ">= 70.5"
      }
    ]
  },
  {
    "conclusion": {
      "description": "",
      "title": "ckd "
    },
    "description": "Random Forest",
    "title": "Rule4",
    "conditions": [
      {
        "description": "",
        "key": "pc",
        "value": "normal",
        "operator": "="
      },
      {
        "description": "",
        "key": "sg",
        "value": "1.02",
        "operator": "<"
      },
      {
        "description": "",
        "key": "bp",
        "value": "65",
        "operator": ">="
      },
      {
        "description": "",
        "key": "age",
        "value": "47.5",
        "operator": "<"
      },
      {
        "description": "",
        "key": "bp",
        "value": "",
        "operator": "< 75"
      }
    ]
  },
  {
    "conclusion": {
      "description": "",
      "title": "ckd "
    },
    "description": "Random Forest",
    "title": "Rule5",
    "conditions": [
      {
        "description": "",
        "key": "pc",
        "value": "normal",
        "operator": "="
      },
      {
        "description": "",
        "key": "sg",
        "value": "1.02",
        "operator": "<"
      },
      {
        "description": "",
        "key": "bp",
        "value": "65",
        "operator": ">="
      },
      {
        "description": "",
        "key": "age",
        "value": "47.5",
        "operator": "<"
      },
      {
        "description": "",
        "key": "bp",
        "value": "75",
        "operator": ">="
      },
      {
        "description": "",
        "key": "age",
        "value": "",
        "operator": "< 46.5"
      }
    ]
  },

    
    
];



  export {
    shortSample
  };